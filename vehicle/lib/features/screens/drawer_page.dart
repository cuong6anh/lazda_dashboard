import 'package:flutter/material.dart';
import 'package:responsive_dashboard/constants.dart';

import '../../constants.dart';

import '../../responsive.dart';

class DrawerPage extends StatefulWidget {
  const DrawerPage({Key? key}) : super(key: key);

  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class ButtonsInfo {
  late String title;
  late IconData icon;
  ButtonsInfo({required this.title, required this.icon});
}

int _currentIndex = 0;

List<ButtonsInfo> _buttonNames = [
  ButtonsInfo(title: "Cứu hộ", icon: Icons.warning_amber),
  ButtonsInfo(title: "Xe", icon: Icons.two_wheeler_rounded),
  ButtonsInfo(title: "Trạm Pin", icon: Icons.ev_station_rounded),
  ButtonsInfo(title: "Pack Pin", icon: Icons.battery_full_outlined),
  ButtonsInfo(title: "Đổi Pin", icon: Icons.earbuds_battery_outlined),
  ButtonsInfo(title: "Đăng xuất", icon: Icons.supervised_user_circle_rounded),
];

class _DrawerPageState extends State<DrawerPage> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: primaryColor,
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(defaultPadding),
          child: Column(
            children: [
              Row(
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/logo_selex1.png',
                    height: 80,
                    width: 80,
                  ),
                  Text("Selex Motor",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.white, fontSize: 25)),
                ],
              ),
              SizedBox(
                height: 130,
              ),
              ...List.generate(
                  _buttonNames.length,
                  (index) => Column(
                        children: [
                          Container(
                            decoration: index == _currentIndex
                                ? BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    gradient: LinearGradient(
                                      colors: [
                                        bluedark.withOpacity(0.9),
                                        bluebland.withOpacity(0.9),
                                      ],
                                    ),
                                  )
                                : null,
                            child: ListTile(
                              title: Text(
                                _buttonNames[index].title,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              leading: Padding(
                                padding: EdgeInsets.all(defaultPadding),
                                child: Icon(_buttonNames[index].icon,
                                    color: Colors.white),
                              ),
                              onTap: () {
                                setState(() {
                                  _currentIndex = index;
                                });
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                          ),
                          Divider(
                            color: Colors.white,
                            thickness: 0.1,
                          )
                        ],
                      )),
            ],
          ),
        ),
      ),
    );
  }
}

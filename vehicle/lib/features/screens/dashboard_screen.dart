import 'package:flutter/material.dart';
import 'package:responsive_dashboard/constants.dart';
import 'package:responsive_dashboard/features/google_map/call_api_vehicle_position.dart';

import 'package:responsive_dashboard/features/vehicle/list_vehicle.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      // child: SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(defaultPadding),
        child: Row(
          children: const [
            Expanded(
              flex: 3,
              child: ApiGoogleMap(),
            ),
            Expanded(flex: 2, child: ListVehicle()),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:responsive_dashboard/features/screens/dashboard_screen.dart';
import 'package:responsive_dashboard/features/vehicle/list_vehicle.dart';

import '../../constants.dart';
import '../../responsive.dart';
import 'drawer_page.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerPage(),
      body: SafeArea(
        child: Row(
          children: [
            if (Responsive.isDesktop(context))
              Expanded(
                flex: 1,
                child: DrawerPage(),
              ),
            Expanded(flex: 5, child: DashboardScreen()),
          ],
        ),
      ),
    );
  }
}

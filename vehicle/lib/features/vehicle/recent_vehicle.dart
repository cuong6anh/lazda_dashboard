import 'dart:js';

import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_dashboard/data/datasourse/vehicle_api_datasourse.dart';
import 'package:responsive_dashboard/data/models/vehicle_models.dart';

//import 'package:flutter_svg/svg.dart';
import '../../constants.dart';
import 'infor_vehicle.dart';

class RecentVehicle extends StatelessWidget {
  const RecentVehicle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }

  FutureBuilder<List<User>> _buildBody(BuildContext context) {
    final HttpService httpService = HttpService();
    return FutureBuilder<List<User>>(
        future: httpService.getPosts(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final List<User>? posts = snapshot.data;

            return Container(
              padding: const EdgeInsets.all(defaultPadding),
              decoration: const BoxDecoration(
                color: bluebland,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: _buildPosts(context, posts!),
            );
          } else if (snapshot.hasError) {
            return Text(
              '${snapshot.error}',
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  _buildPosts(BuildContext context, List<User> posts) {
    return Column(
      children: [
        Text(
          "Tổng số xe ${posts.length}",
          style: Theme.of(context).textTheme.subtitle1,
        ),
        SizedBox(
          width: double.infinity,
          height: 600,
          child: SingleChildScrollView(
              child: DataTable(
            columns: const [
              DataColumn(
                label: Text("ID Xe"),
              ),
              DataColumn(
                label: Text("Trạng Thái"),
              ),
              DataColumn(
                label: Text("Tên Hub"),
              ),
            ],
            rows: List.generate(posts.length,
                (index) => recentFileDataRow(posts[index], context)),
          )),
        ),
      ],
    );
  }
}

DataRow recentFileDataRow(User userInfo, BuildContext context) {
  return DataRow(
    cells: [
      DataCell(
          Row(
            children: [
              Image.asset(
                'assets/icons/${userInfo.vehicle_type}.png',
                height: 60,
                width: 60,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                child: Text(userInfo.id),
              ),
            ],
          ), onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => InForVehicle(userInfo: userInfo)));
      }),
      DataCell(Text(userInfo.status)),
      DataCell(Text(userInfo.vehicle_hub)),
    ],
  );
}

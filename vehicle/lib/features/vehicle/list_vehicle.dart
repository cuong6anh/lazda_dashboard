import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../constants.dart';
import 'drop_down.dart';
import 'recent_vehicle.dart';
import 'search_field.dart';

class ListVehicle extends StatefulWidget {
  const ListVehicle({Key? key}) : super(key: key);

  @override
  _ListVehicleState createState() => _ListVehicleState();
}

class _ListVehicleState extends State<ListVehicle> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SizedBox(
        height: 900,
        width: double.infinity,
        child: Container(
          padding:
              EdgeInsets.only(left: defaultPadding, bottom: defaultPadding),
          child: Column(
            children: [
              Column(
                children: [
                  SearchField(),
                  SizedBox(
                    height: defaultPadding,
                  ),
                  DropDown(),
                  SizedBox(
                    height: defaultPadding,
                  ),
                ],
              ),
              Expanded(
                child: RecentVehicle(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

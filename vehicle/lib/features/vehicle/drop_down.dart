import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:responsive_dashboard/constants.dart';

import '../../constants.dart';

class DropDown extends StatefulWidget {
  const DropDown({Key? key}) : super(key: key);

  @override
  _DropDownState createState() => _DropDownState();
}

class _DropDownState extends State<DropDown> {
  String dropdownvalue = 'Cầu Giấy';
  String dropdownvalue1 = 'On';

  // List of items in our dropdown menu
  var hub = [
    'Cầu Giấy',
    'Giải Phóng',
    'Long Biên',
    'Gia Lâm',
    'Ba Đình',
  ];
  var status = [
    'On',
    'Off',
  ];
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(defaultPadding)),
                child: SizedBox(
                  width: 200,
                  child: DropdownSearch<String>(
                    //validator: (v) => v == null ? "required field" : null,
                    dropdownSearchDecoration: InputDecoration(
                      labelText: "Chọn Hub *",
                      contentPadding: EdgeInsets.fromLTRB(12, 12, 0, 0),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(defaultPadding)),
                    ),
                    mode: Mode.MENU,
                    popupBackgroundColor: bluebland,

                    showSelectedItems: true,
                    items: hub,
                    showClearButton: true,
                    onChanged: print,
                    clearButtonSplashRadius: 20,
                    onBeforeChange: (a, b) {
                      if (b == null) {
                        AlertDialog alert = AlertDialog(
                          title: Text("Are you sure..."),
                          content: Text("...you want to clear the selection"),
                          actions: [
                            TextButton(
                              child: Text("OK"),
                              onPressed: () {
                                Navigator.of(context).pop(true);
                              },
                            ),
                            TextButton(
                              child: Text("NOT OK"),
                              onPressed: () {
                                Navigator.of(context).pop(false);
                              },
                            ),
                          ],
                        );

                        return showDialog<bool>(
                            context: context,
                            builder: (BuildContext context) {
                              return alert;
                            });
                      }

                      return Future.value(true);
                    },
                  ),
                ))),
        SizedBox(
          width: 30,
        ),
        Expanded(
            child: Container(
          decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(defaultPadding)),
          child: SizedBox(
            width: 200,
            child: DropdownSearch<String>(
              //validator: (v) => v == null ? "required field" : null,
              dropdownSearchDecoration: InputDecoration(
                labelText: "Trạng Thái *",
                contentPadding: EdgeInsets.fromLTRB(12, 12, 0, 0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(defaultPadding)),
              ),
              mode: Mode.MENU,
              popupBackgroundColor: bluebland,
              showSelectedItems: true,
              items: status,
              showClearButton: true,
              onChanged: print,
              clearButtonSplashRadius: 20,
              onBeforeChange: (a, b) {
                if (b == null) {
                  AlertDialog alert = AlertDialog(
                    title: Text("Are you sure..."),
                    content: Text("...you want to clear the selection"),
                    actions: [
                      TextButton(
                        child: Text("OK"),
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                      ),
                      TextButton(
                        child: Text("NOT OK"),
                        onPressed: () {
                          Navigator.of(context).pop(false);
                        },
                      ),
                    ],
                  );

                  return showDialog<bool>(
                      context: context,
                      builder: (BuildContext context) {
                        return alert;
                      });
                }

                return Future.value(true);
              },
            ),
          ),
        )),
      ],
    );
  }
}

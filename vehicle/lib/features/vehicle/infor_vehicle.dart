import 'package:flutter/material.dart';
import 'package:responsive_dashboard/data/models/vehicle_models.dart';

import '../../constants.dart';

class InForVehicle extends StatelessWidget {
  late User userInfo;
  InForVehicle({Key? key, required this.userInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Xe ${userInfo.id}',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: themeColor,
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Info(),
            ),
            Expanded(
              flex: 2,
              child: HistoryVehicle(),
            ),
          ],
        ),
      ),
    );
  }

  Row HistoryVehicle() {
    return Row(
      children: [
        Expanded(
          child: Container(
            margin: EdgeInsets.only(top: defaultPadding),
            padding: EdgeInsets.all(defaultPadding),
            decoration: BoxDecoration(
              color: Colors.blue,
              border:
                  Border.all(width: 2, color: primaryColor.withOpacity(0.5)),
              borderRadius: BorderRadius.all(Radius.circular(defaultPadding)),
            ),
            child: Text(
              'Lịch sử đổi pin',
              style: TextStyle(fontSize: 30),
            ),
          ),
        ),
        SizedBox(
          width: 30,
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(top: defaultPadding),
            padding: EdgeInsets.all(defaultPadding),
            decoration: BoxDecoration(
              color: Colors.blue,
              border:
                  Border.all(width: 2, color: primaryColor.withOpacity(0.5)),
              borderRadius: BorderRadius.all(Radius.circular(defaultPadding)),
            ),
            child: Text(
              'Lịch sử cứu hộ',
              style: TextStyle(fontSize: 30),
            ),
          ),
        ),
      ],
    );
  }

  Container Info() {
    return Container(
      child: Center(
        child: SizedBox(
          height: 300,
          width: 400,
          child: Container(
            margin: EdgeInsets.only(top: defaultPadding),
            padding: EdgeInsets.all(defaultPadding),
            decoration: BoxDecoration(
              color: Colors.blue,
              border:
                  Border.all(width: 2, color: primaryColor.withOpacity(0.5)),
              borderRadius: BorderRadius.all(Radius.circular(defaultPadding)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/icons/${userInfo.vehicle_type}.png',
                      height: 100,
                      width: 100,
                    ),
                    Text(
                      'Xe ${userInfo.id}',
                      style: TextStyle(fontSize: 30),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const [
                    Text(
                      'Trạng Thái',
                      style: TextStyle(fontSize: 30),
                    ),
                    Text(
                      'Tên Hub',
                      style: TextStyle(fontSize: 30),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      '${userInfo.status}',
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      '${userInfo.vehicle_hub}',
                      style: TextStyle(fontSize: 20),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:responsive_dashboard/data/datasourse/vehicle_position_api_datasourse.dart';
import 'package:responsive_dashboard/data/models/vehicle_position.dart';
import 'package:google_maps/google_maps.dart';
import '../../constants.dart';

import 'dart:html';
import 'dart:ui' as ui;

class ApiGoogleMap extends StatelessWidget {
  const ApiGoogleMap({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }

  FutureBuilder<List<VehiclePosition>> _buildBody(BuildContext context) {
    final HttpService httpService = HttpService();
    return FutureBuilder<List<VehiclePosition>>(
        future: httpService.getPosts(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final List<VehiclePosition>? position = snapshot.data;
            return SingleChildScrollView(
                child: SizedBox(
                    height: 900,
                    width: double.infinity,
                    child: getMap(context, position!)));
          } else if (snapshot.hasError) {
            return Text(
              '${snapshot.error}',
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        });
  }
}

Widget getMap(BuildContext context, List<VehiclePosition> list) {
  String htmlId = "6";

  //ignore: undefined_prefixed_name
  ui.platformViewRegistry.registerViewFactory(htmlId, (int viewId) {
    final latLang = LatLng(21.007415, 105.839875);
    //class to create a div element

    final mapOptions = MapOptions()
      ..zoom = 15
      ..minZoom = 13
      ..tilt = 90
      ..center = latLang;
    final elem = DivElement()
      ..id = htmlId
      ..style.width = "100%"
      ..style.height = "100%"
      ..style.border = "none";

    final map = GMap(elem, mapOptions);

    for (int i = 0; i < list.length; i++) {
      Marker(MarkerOptions()
        ..position = LatLng(list[i].lat, list[i].lng)
        ..map = map
        ..title = 'My position'
        ..label = list[i].vehicleId);
    }

    return elem;
  });
  //creates a platform view for Flutter Web
  return HtmlElementView(
    viewType: htmlId,
  );
}

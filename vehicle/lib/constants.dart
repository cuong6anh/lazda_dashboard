import 'package:flutter/material.dart';

const primaryColor = Color(0xFF1565C0);
const secondaryColor = Color(0xFF2A2D3E);
const bgColor = Color(0xFF212332);
const themeColor = Color(0xFFBBDEFB);

const defaultPadding = 16.0;

//const Color purpleLight = Color(0XFF1e224c);
//const Color purpleDark = Color(0XFF0d193e);
const Color bluebland = Color(0XFF448AFF);
const Color bluedark = Color(0XFF0D47A1);

import 'dart:convert';
import 'package:http/http.dart';
import 'package:responsive_dashboard/data/models/vehicle_models.dart';

//http://localhost:3000/vehicles
//https://fake-api-vehicle.herokuapp.com/vehicles/100
class HttpService {
  final String postsURL = "http://localhost:3000/vehicles";

  Future<List<User>> getPosts() async {
    Response res = await get(Uri.parse(postsURL));

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<User> posts = body
          .map(
            (dynamic item) => User.fromJson(item),
          )
          .toList();

      return posts;
    } else {
      throw "Unable to retrieve posts.";
    }
  }
}

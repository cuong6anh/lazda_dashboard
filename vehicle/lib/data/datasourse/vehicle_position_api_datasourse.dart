import 'dart:convert';
import 'package:http/http.dart';
import 'package:responsive_dashboard/data/models/vehicle_position.dart';

class HttpService {
  final String postsURL = "http://localhost:3001/vehicles_latlng";

  Future<List<VehiclePosition>> getPosts() async {
    Response res = await get(Uri.parse(postsURL));

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<VehiclePosition> position = body
          .map(
            (dynamic item) => VehiclePosition.fromJson(item),
          )
          .toList();

      return position;
    } else {
      throw "Unable to retrieve posts.";
    }
  }
}

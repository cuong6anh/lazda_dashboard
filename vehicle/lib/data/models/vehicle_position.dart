import 'dart:convert';

List<VehiclePosition> userFromJson(String str) => List<VehiclePosition>.from(
    json.decode(str).map((x) => VehiclePosition.fromJson(x)));
String userToJson(List<VehiclePosition> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class VehiclePosition {
  VehiclePosition({
    required this.vehicleId,
    required this.lat,
    required this.lng,
  });

  String vehicleId;
  double lat;
  double lng;

  factory VehiclePosition.fromJson(Map<String, dynamic> json) =>
      VehiclePosition(
        vehicleId: json["vehicleId"],
        lat: json["lat"],
        lng: json["lng"],
      );
  Map<String, dynamic> toJson() =>
      {"vehicleId": vehicleId, "lat": lat, "lng": lng};
}

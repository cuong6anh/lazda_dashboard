import 'dart:convert';

List<User> userFromJson(String str) =>
    List<User>.from(json.decode(str).map((x) => User.fromJson(x)));
String userToJson(List<User> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class User {
  User({
    required this.status,
    required this.id,
    required this.vehicle_hub,
    required this.vehicle_type,
  });
  String vehicle_type;
  String id;
  String vehicle_hub;
  String status;

  factory User.fromJson(Map<String, dynamic> json) => User(
        status: json["status"],
        id: json["id"],
        vehicle_type: json["vehicle_type"],
        vehicle_hub: json["vehicle_hub"],
      );
  Map<String, dynamic> toJson() => {
        "status": status,
        "id": id,
        " vehicle_type": vehicle_type,
        "vehicle_hub": vehicle_hub,
      };
}
